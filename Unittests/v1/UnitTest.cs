﻿// <copyright file="UnitTest.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Unittests
{
    using System.IO;
    using System.Linq;
    using System.Text;
    using NUnit.Framework;

    /// <summary>
    /// UnitTest
    /// </summary>
    [TestFixture]
    public class UnitTest
    {
        /// <summary>
        /// SaveAndLoadeFileSame
        /// </summary>
        [Test]
        public void SaveAndLoadeFileSame()
        {
            var upLoadeFile = new Monosoft.Service.SecureTransfer.V1.DTO.File();
            upLoadeFile.timeToLiveInDays = 1;
            upLoadeFile.fileName = "test";
            upLoadeFile.fileType = ".txt";
            upLoadeFile.fileData = Encoding.ASCII.GetBytes("dette er file data").ToList();
            var creatFilePath = new Monosoft.Service.SecureTransfer.V1.DTO.SaveFileInfo() { filePath = Monosoft.Service.SecureTransfer.V1.FileUpload.SaveFile(upLoadeFile) };
            var returnFile = Monosoft.Service.SecureTransfer.V1.FileDownload.DownloadFile(creatFilePath);

            Assert.AreEqual(upLoadeFile.timeToLiveInDays, returnFile.timeToLiveInDays, "timeToLiveInDays");
            Assert.AreEqual(upLoadeFile.fileName, returnFile.fileName, "fileName");
            Assert.AreEqual(upLoadeFile.fileType, returnFile.fileType, "fileType");
            Assert.AreEqual(upLoadeFile.fileData, returnFile.fileData, "fileData");
        }

        /// <summary>
        /// Cleanup
        /// </summary>
        [Test]
        public void Cleanup()
        {
            var upLoadeFile = new Monosoft.Service.SecureTransfer.V1.DTO.File();
            upLoadeFile.timeToLiveInDays = -1;
            upLoadeFile.fileName = "test";
            upLoadeFile.fileType = ".txt";
            upLoadeFile.fileData = Encoding.ASCII.GetBytes("dette er file data").ToList();
            var creatFilePath = new Monosoft.Service.SecureTransfer.V1.DTO.SaveFileInfo() { filePath = Monosoft.Service.SecureTransfer.V1.FileUpload.SaveFile(upLoadeFile) };
            Monosoft.Service.SecureTransfer.V1.CleanUp.DeleteOldFile();
            FileInfo info = new FileInfo($"{creatFilePath}");
            var returnString = string.Empty;
            if (info.Exists)
            {
                returnString = "file exists";
            }
            else
            {
                returnString = "file dose not exists";
            }

            Assert.AreEqual("file dose not exists", returnString, "file should not exists");
        }
    }
}
