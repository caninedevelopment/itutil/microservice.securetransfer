﻿// <copyright file="FileUpload.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Service.SecureTransfer.V1
{
    using System;
    using System.IO;

    /// <summary>
    /// FileUpload
    /// </summary>
    public static class FileUpload
    {
        /// <summary>
        /// SaveFile
        /// </summary>
        /// <param name="file">File</param>
        /// <returns>string</returns>
        public static string SaveFile(Monosoft.Service.SecureTransfer.V1.DTO.File file)
        {
            if (file == null)
            {
                throw new ArgumentNullException(nameof(file));
            }

            // LONG: MAX: 9,223,372,036,854,775,807
            var folderDate = DateTime.Today.AddDays(file.timeToLiveInDays);
            var folderName = folderDate.Ticks.ToString(ITUtil.Common.Constants.Culture.DefaultCulture).PadLeft(20, '0');
            string folderPath = $"./UploadeFiles/{folderName}";

            var res = Guid.NewGuid();

            var filePath = $"{folderPath}/{res}";

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(file);

            try
            {
                // Determine whether the directory exists.
                if (Directory.Exists(folderPath) == false)
                {
                    Directory.CreateDirectory(folderPath);
                }

                if (System.IO.File.Exists(filePath) == false)
                {
                    System.IO.File.WriteAllText(filePath, json);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }

            return filePath;
        }
    }
}
