﻿// <copyright file="CleanUp.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Service.SecureTransfer.V1
{
    using System;
    using System.IO;

    /// <summary>
    /// CleanUp
    /// </summary>
    public static class CleanUp
    {
        /// <summary>
        /// DeleteOldFile
        /// </summary>
        public static void DeleteOldFile()
        {
            var toDay = DateTime.Today.Ticks.ToString(ITUtil.Common.Constants.Culture.DefaultCulture).PadLeft(20, '0');
            var directories = Directory.GetDirectories($"./UploadeFiles");
            foreach (var item in directories)
            {
                DirectoryInfo di = new DirectoryInfo(item);
                if (string.Compare(di.Name, toDay, StringComparison.OrdinalIgnoreCase) <= 0)
                {
                    Directory.Delete(item, recursive: true);
                }
            }
        }
    }
}
