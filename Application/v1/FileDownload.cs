﻿// <copyright file="FileDownload.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Service.SecureTransfer.V1
{
    using System;
    using System.IO;
    using Newtonsoft.Json;

    /// <summary>
    /// FileDownload
    /// </summary>
    public static class FileDownload
    {
        /// <summary>
        /// DownloadFile
        /// </summary>
        /// <param name="secureID">SaveFileInfo</param>
        /// <returns>File</returns>
        public static Monosoft.Service.SecureTransfer.V1.DTO.File DownloadFile(Monosoft.Service.SecureTransfer.V1.DTO.SaveFileInfo secureID)
        {
            if (secureID == null)
            {
                throw new ArgumentNullException(nameof(secureID));
            }

            FileInfo info = new FileInfo($"{secureID.filePath}");
            var returnFile = new Monosoft.Service.SecureTransfer.V1.DTO.File();
            if (info.Exists)
            {
                var json = System.IO.File.ReadAllText(info.FullName);
                returnFile = JsonConvert.DeserializeObject<Monosoft.Service.SecureTransfer.V1.DTO.File>(json);
                File.Delete(info.FullName);
            }

            return returnFile;
        }
    }
}
