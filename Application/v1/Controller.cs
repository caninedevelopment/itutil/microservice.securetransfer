﻿// <copyright file="Controller.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Microservice
{
    using System;
    using System.Collections.Generic;
    using ITUtil.Common.DTO;
    using ITUtil.Common.Utils.Bootstrapper;
    using static ITUtil.Common.Utils.Bootstrapper.OperationDescription;

    /// <summary>
    /// Controller
    /// </summary>
    public class Controller : IMicroservice
    {
        /// <inheritdoc/>
        public string ServiceName
        {
            get
            {
                return "SecureTransfer";
            }
        }

        /// <inheritdoc/>
        public ProgramVersion ProgramVersion
        {
            get
            {
                return new ProgramVersion("1.0.0.0");
            }
        }

        private static readonly MetaDataDefinition UploaderClaim = new MetaDataDefinition("Monosoft.Service.SecureTransfer", "uploader", MetaDataDefinition.DataContextEnum.organisationClaims, new LocalizedString("en", "User is allowed to upload files"));

        /// <summary>
        /// Gets OperationDescription
        /// </summary>
        public List<OperationDescription> OperationDescription
        {
            get
            {
                return new List<OperationDescription>()
                {
                       new OperationDescription(
                           "CleanUp",
                           ".....",
                           null,
                           null,
                           null,
                           ExecuteOperationCleanUp),
                       new OperationDescription(
                           "UploadFile",
                           ".....",
                           typeof(Monosoft.Service.SecureTransfer.V1.DTO.File),
                           typeof(Monosoft.Service.SecureTransfer.V1.DTO.SaveFileInfo),
                           new MetaDataDefinitions(new MetaDataDefinition[] { UploaderClaim }),
                           ExecuteOperationUploadFile),
                       new OperationDescription(
                           "DownloadFile",
                           ".....",
                           typeof(Monosoft.Service.SecureTransfer.V1.DTO.SaveFileInfo),
                           typeof(Monosoft.Service.SecureTransfer.V1.DTO.File),
                           null,
                           ExecuteOperationDownloadFile),
                };
            }
        }

        /// <summary>
        /// ExecuteOperationCleanUp
        /// </summary>
        /// <param name="wrapper">MessageWrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationCleanUp(MessageWrapper wrapper)
        {
            Monosoft.Service.SecureTransfer.V1.CleanUp.DeleteOldFile();

            return ReturnMessageWrapper.CreateResult(true, wrapper, null, LocalizedString.OK);
        }

        /// <summary>
        /// ExecuteOperationUploadFile
        /// </summary>
        /// <param name="wrapper">MessageWrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationUploadFile(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var msg = ITUtil.Common.Utils.MessageDataHelper.FromMessageData<Monosoft.Service.SecureTransfer.V1.DTO.File>(wrapper.messageData);
            if (msg.fileData.Count > 0)
            {
                var res = new Monosoft.Service.SecureTransfer.V1.DTO.SaveFileInfo() { filePath = Monosoft.Service.SecureTransfer.V1.FileUpload.SaveFile(msg) };

                return ReturnMessageWrapper.CreateResult(true, wrapper, res, LocalizedString.OK);
            }
            else
            {
                return ReturnMessageWrapper.CreateResult(true, wrapper, null, LocalizedString.Create("EN", "No file found"));
            }
        }

        /// <summary>
        /// ExecuteOperationDownloadFile
        /// </summary>
        /// <param name="wrapper">MessageWrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationDownloadFile(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var msg = ITUtil.Common.Utils.MessageDataHelper.FromMessageData<Monosoft.Service.SecureTransfer.V1.DTO.SaveFileInfo>(wrapper.messageData);

            var res = Monosoft.Service.SecureTransfer.V1.FileDownload.DownloadFile(msg);

            return ReturnMessageWrapper.CreateResult(true, wrapper, res, LocalizedString.OK);
        }
    }
}
